# Nedap TA



## Summary

Here you can find the solution for the technical assessment for the android development vacancy at Nedap.

This is a simple Android application that fetches and displays a list of users
from a given API. The users are displayed in a list view. Additionally, users can be selected
and viewed on a separate screen. Local storage is used to cache the user data. The application supports Android 10+.

## UI & UX

App implementation was based on the following requirements:

1. **Setup**:
- Create a new application project (Using the latest technologies).
2. **Fetching and Storing Users**:
- Upon app launch, fetch the list of users from `https://randomuser.me/api/?results=100`.
- Implement error handling for potential network or data issues.
- Store the fetched user data in a database.
- Support offline mode. Previously stored data should be displayed if not possible to retrieve
new data.
3. **Rendering the User List**:
- Create a screen (Screen 1) to display the fetched users list.
- Display the user's thumbnail image, name, and location.
- Refresh the list and the cache once the new data is fetched after pulling to refresh
4. **User Selection and Details**:
- Allow users to be selected from Screen 1.
- Create another screen (Screen 2) showcasing the selected user’s medium image. You're
free to choose which additional fields to showcase.
- Show the large image when the medium image is tapped (Screen 3).
5. **Settings**:
- Show a fake settings screen (Screen 4) using flat navigation (navigation drawer on Android).
- Views hierarchy:
Screen 1 -> Screen 2 -> Screen 3
Screen 4
6. **Testing**:
- Implement relevant tests for your application to ensure it functions as intended. This can be
a mix of unit tests, UI tests, or any other suitable testing methodology.

## Recording

![recording](./attachments/ezgif.com-video-to-gif-converter.gif)

## Integrated with the help of such tools

- 👉 **Kotlin Coroutines**: For efficient asynchronous programming.
- 👉 **Kotlin Flow**: Managing async data streams.
- 👉 **Hilt**: For injecting dependencies.
- 👉 **Retrofit &OkHttp Client**: Handling APIs.
- 👉 **Jetpack Compose UI & Compose-Navigation**: Crafting UI & navigation.
- 👉 **Coil-Compose**: Loading images asynchronously.
- 👉 **Room**: Local database management and offline mode.

## Test

Providing basic unit testing for insurance of seamless work.

WIP: trying out new features in testing area, so this part will be updating permanently. 