package com.example.nedapta

import com.example.nedapta.utils.UiState
import com.example.nedapta.utils.asUiState
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Test

//import org.junit.jupiter.api.Assertions.assertTrue
//import org.junit.jupiter.api.Test

class UiStateTest {

    @Test
    fun `asAsyncResult emits Success with data`() = runTest {
        val flow = flowOf("data").asUiState().toList()
        val result = flow.first()
        assertTrue(result is UiState.Success && result.data == "data")
    }

    @Test
    fun `asAsyncResult emits Error on exception`() = runTest {
        val flow = flow<String> { throw Exception("Error") }.asUiState().toList()
        val result = flow.first()
        assertTrue(result is UiState.Error && result.message == "Error")
    }

    @Test
    fun `asAsyncResult emits Error`() = runTest {
        val flow = flow { emit(UiState.Error("Error")) }.toList()
        val result = flow.first()
        assertTrue(result.message == "Error")
    }
}
