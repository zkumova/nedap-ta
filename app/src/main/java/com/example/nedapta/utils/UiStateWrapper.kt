package com.example.nedapta.utils

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun <T> UiStateWrapper(
    uiState: UiState<T>,
    onError: @Composable (String) -> Unit = { message ->
        SimpleCenteredText(message = message)
    },
    onLoading: @Composable () -> Unit = {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    },
    onEmpty: @Composable (String) -> Unit = {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(
                text = it,
                modifier = Modifier.padding(16.dp),
                textAlign = TextAlign.Center
            )
        }
    },
    onSuccess: @Composable (T) -> Unit
) {
    when (uiState) {
        is UiState.Loading -> onLoading()
        is UiState.Success -> onSuccess(uiState.data)
        is UiState.Error -> onError(uiState.message ?: "An error occurred")
        is UiState.Empty -> onEmpty(uiState.message ?: "No data to show")
    }
}