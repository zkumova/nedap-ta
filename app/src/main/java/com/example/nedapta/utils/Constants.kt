package com.example.nedapta.utils

object Constants {

    const val DB = "UsersListDB"
    const val USER_TABLE = "user_table"

    const val BASE_URL = "https://randomuser.me/api/"
    const val LIST_AMOUNT = 100
    const val LOG_TAG = "Nedap"
    const val NAV_GENERAL_ERROR_MESSAGE = "Oops...\n No such user identifier found"

    const val ID_NAME = "name"
    const val ID_AGE = "age"
    const val ID_EMAIL = "email"
    const val ID_NATIONALITY = "nationality"
    const val ID_LOCATION = "location"
    const val ID_IMAGE = "image"
    const val ID_BOTTOM_TEXT = "bottom_text"
}
