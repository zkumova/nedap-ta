package com.example.nedapta.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import com.example.nedapta.data.db.DbUser
import com.example.nedapta.data.remote.models.UserInfo
import com.example.nedapta.ui.settings.MenuItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf

val menuItemsList = listOf(
    MenuItem(
        id = "menu 1",
        title = "Menu 1",
        contentDescription = "Go to menu item 1",
        imageIcon = Icons.Default.Build
    ),
    MenuItem(
        id = "menu 2",
        title = "Menu 2",
        contentDescription = "Go to menu item 2",
        imageIcon = Icons.Default.Build
    ),
    MenuItem(
        id = "menu 3",
        title = "Menu 3",
        contentDescription = "Go to menu item 3",
        imageIcon = Icons.Default.Build
    ),
    MenuItem(
        id = "menu 4",
        title = "Menu 4",
        contentDescription = "Go to menu item 4",
        imageIcon = Icons.Default.Build
    ),
    MenuItem(
        id = "menu 5",
        title = "Menu 5",
        contentDescription = "Go to menu item 5",
        imageIcon = Icons.Default.Build
    )
)

val usersMockList = listOf(
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6313",
            name = "Zhanna Kumova",
            age = 33,
            email = "zhanna.kumova13@gmail.com",
            nationality = "UA",
            location = "Arnhem, The Netherlands",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6312",
            name = "Alex Perov",
            age = 34,
            email = "alex.perov13@gmail.com",
            nationality = "UA",
            location = "Lviv, Ukraine",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6311",
            name = "Marina Kravchenko",
            age = 35,
            email = "marina.kravchenko13@gmail.com",
            nationality = "UA",
            location = "Krakow, Poland",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6310",
            name = "Nastia Ulianitskaya",
            age = 36,
            email = "nastia.ulianitskaya13@gmail.com",
            nationality = "UA",
            location = "Krakow, Poland",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6309",
            name = "Ashour Makdis",
            age = 37,
            email = "ashour.makdis13@gmail.com",
            nationality = "UA",
            location = "Arnhem, The Netherlands",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6308",
            name = "Kirill Kumov",
            age = 38,
            email = "kirill.kumov13@gmail.com",
            nationality = "UA",
            location = "Calgary, Alberta, Canada",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6307",
            name = "Anna Kumova",
            age = 39,
            email = "anna.kumova13@gmail.com",
            nationality = "UA",
            location = "Calgary, Alberta, Canada",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        ),
        DbUser(
            id = "cf75545b-0f27-4ba5-b97f-e98a7c7d6306",
            name = "Naruto Uzumaki",
            age = 40,
            email = "naruto.uzumaki13@gmail.com",
            nationality = "UA",
            location = "Village Hidden in the Leaves, Land of Fire",
            imageSmall = "https://randomuser.me/api/portraits/women/47.jpg",
            imageMedium = "https://randomuser.me/api/portraits/med/women/47.jpg",
            imageLarge = "https://randomuser.me/api/portraits/thumb/women/47.jpg",
        )
)
