package com.example.nedapta.utils

import android.util.Log
import com.example.nedapta.data.remote.models.UserInfo
import com.example.nedapta.data.remote.responses.UserListResponse

fun UserListResponse.toUserInfo(): List<UserInfo> {
    val mutableList = mutableListOf<UserInfo>()
    Log.d(Constants.LOG_TAG, this.results.size.toString())
    this.results.forEach { user ->
        Log.d(Constants.LOG_TAG, user.email)
        mutableList.add(
            UserInfo(
                uuid = user.login.uuid,
                name = "${user.name.title} ${user.name.first} ${user.name.last}",
                age = user.dob.age,
                email = user.email,
                nationality = user.nat,
                location = "${user.location.city}, ${user.location.state}, ${user.location.country}",
                imageSmall = user.picture.thumbnail,
                imageMedium = user.picture.medium,
                imageLarge = user.picture.large
            )
        )
    }
    Log.d(Constants.LOG_TAG, mutableList.size.toString())
    return mutableList.toList()
}
