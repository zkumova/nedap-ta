package com.example.nedapta.data

import com.example.nedapta.data.db.DbUser
import com.example.nedapta.data.db.DbUser.Companion.toDbUser
import com.example.nedapta.data.db.UsersInfoDbRepo
import com.example.nedapta.data.remote.UserListRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class DataRepoImpl @Inject constructor(
    private val apiRepo: UserListRepo,
    private val dbRepo: UsersInfoDbRepo
): DataRepo {
    override suspend fun getUsersData(
        amount: Int,
        isConnectedToNetwork: Boolean
    ): Flow<List<DbUser>> =
        when(isConnectedToNetwork){
            true -> {
                getUsersFromLocalStorage()
                    .onStart {
                        cleanLocalDb()
                        fetchAndSave(amount)
                    }
            }
            false -> { getUsersFromLocalStorage() }
        }

    override suspend fun getUserData(id: String): Flow<DbUser> =
        dbRepo.getUserInfoFromDbRepo(id)

    private suspend fun fetchAndSave(amount: Int) {
        val remoteData = apiRepo.getUserList(amount)
        dbRepo.addUsersToDbRepo(remoteData.map { it.toDbUser() })
    }

    private suspend fun getUsersFromLocalStorage(): Flow<List<DbUser>> =
        dbRepo.getUsersInfoListFromDbRepo()

    private suspend fun cleanLocalDb() {
        dbRepo.deleteAllUsersFromDbRepo()
    }
}