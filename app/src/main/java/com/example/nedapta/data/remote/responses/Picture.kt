package com.example.nedapta.data.remote.responses

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
)