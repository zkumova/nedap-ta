package com.example.nedapta.data.remote.responses

data class UserListResponse(
    val info: Info,
    val results: List<Result>
)