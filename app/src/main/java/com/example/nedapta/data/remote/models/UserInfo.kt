package com.example.nedapta.data.remote.models

data class UserInfo(
    val uuid: String,
    val name: String,
    val age: Int,
    val email: String,
    val nationality: String,
    val location: String,
    val imageSmall: String,
    val imageMedium: String,
    val imageLarge: String
)
