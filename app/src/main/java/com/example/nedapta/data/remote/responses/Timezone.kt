package com.example.nedapta.data.remote.responses

data class Timezone(
    val description: String,
    val offset: String
)