package com.example.nedapta.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.nedapta.utils.Constants.DB

@Database(entities = [DbUser::class], version = 1, exportSchema = false)
abstract class UsersInfoDB: RoomDatabase() {

    abstract fun userDao(): UserInfoDao

    companion object {
        @Volatile
        private var INSTANCE: UsersInfoDB? = null

        fun getDatabase(context: Context): UsersInfoDB {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UsersInfoDB::class.java,
                    DB
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}