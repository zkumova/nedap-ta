package com.example.nedapta.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.nedapta.data.db.DbUser.Companion.toDbUser
import com.example.nedapta.data.remote.models.UserInfo
import com.example.nedapta.utils.Constants.USER_TABLE

@Entity(tableName = USER_TABLE)
data class DbUser(
    @PrimaryKey
    val id: String,
    val name: String,
    val age: Int,
    val email: String,
    val nationality: String,
    val location: String,
    val imageSmall: String,
    val imageMedium: String,
    val imageLarge: String
) {
    companion object {
        fun UserInfo.toDbUser() = DbUser(
            id = this.uuid,
            name = this.name,
            age = this.age,
            email = this.email,
            nationality = this.nationality,
            location = this.location,
            imageSmall = this.imageSmall,
            imageMedium = this.imageMedium,
            imageLarge = this.imageLarge
        )

        fun DbUser.toUserInfo() = UserInfo(
            uuid = this.id,
            name = this.name,
            age = this.age,
            email = this.email,
            nationality = this.nationality,
            location = this.location,
            imageSmall = this.imageSmall,
            imageMedium = this.imageMedium,
            imageLarge = this.imageLarge
        )
    }
}