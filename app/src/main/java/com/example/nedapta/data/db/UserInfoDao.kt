package com.example.nedapta.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.nedapta.utils.Constants.USER_TABLE
import kotlinx.coroutines.flow.Flow

@Dao
interface UserInfoDao {

    @Query("SELECT * FROM $USER_TABLE")
    fun getUsersInfo(): Flow<List<DbUser>>

    @Query("SELECT * FROM $USER_TABLE WHERE id= :uuid")
    fun getUserInfo(uuid: String): Flow<DbUser>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(usersList: List<DbUser>)

    @Query("DELETE FROM $USER_TABLE")
    suspend fun deleteAll()
}