package com.example.nedapta.data

import com.example.nedapta.data.db.DbUser
import kotlinx.coroutines.flow.Flow

interface DataRepo {

    suspend fun getUsersData(amount: Int, isConnectedToNetwork: Boolean): Flow<List<DbUser>>

    suspend fun getUserData(id: String): Flow<DbUser>
}