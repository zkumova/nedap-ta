package com.example.nedapta.data.remote.responses

data class Street(
    val name: String,
    val number: Int
)