package com.example.nedapta.data.remote

import com.example.nedapta.data.remote.models.UserInfo
import kotlinx.coroutines.flow.Flow

interface UserListRepo {

    suspend fun getUserList(amount: Int): List<UserInfo>
}
