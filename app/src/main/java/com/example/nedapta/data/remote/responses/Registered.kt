package com.example.nedapta.data.remote.responses

data class Registered(
    val age: Int,
    val date: String
)