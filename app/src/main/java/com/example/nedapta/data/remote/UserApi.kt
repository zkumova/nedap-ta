package com.example.nedapta.data.remote

import com.example.nedapta.data.remote.responses.UserListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {

    @GET(".")
    suspend fun getUserList(@Query("results") amount: Int
    ): UserListResponse
}
