package com.example.nedapta.data.remote.responses

data class Name(
    val first: String,
    val last: String,
    val title: String
)