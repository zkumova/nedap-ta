package com.example.nedapta.data.remote.responses

data class Coordinates(
    val latitude: String,
    val longitude: String
)