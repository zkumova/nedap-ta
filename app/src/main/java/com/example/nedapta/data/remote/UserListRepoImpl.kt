package com.example.nedapta.data.remote

import android.util.Log
import com.example.nedapta.data.remote.models.UserInfo
import com.example.nedapta.utils.Constants
import com.example.nedapta.utils.Constants.LIST_AMOUNT
import com.example.nedapta.utils.toUserInfo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UserListRepoImpl @Inject constructor(
    private val api: UserApi
) : UserListRepo {

    override suspend fun getUserList(amount: Int): List<UserInfo> =
        api.getUserList(LIST_AMOUNT).toUserInfo()
}
