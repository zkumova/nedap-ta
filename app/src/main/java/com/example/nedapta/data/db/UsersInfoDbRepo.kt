package com.example.nedapta.data.db

import kotlinx.coroutines.flow.Flow

interface UsersInfoDbRepo {

    suspend fun getUsersInfoListFromDbRepo(): Flow<List<DbUser>>

    suspend fun getUserInfoFromDbRepo(uuid: String): Flow<DbUser>

    suspend fun addUsersToDbRepo(users: List<DbUser>)

    suspend fun deleteAllUsersFromDbRepo()
}