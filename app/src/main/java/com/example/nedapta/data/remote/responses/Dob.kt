package com.example.nedapta.data.remote.responses

data class Dob(
    val age: Int,
    val date: String
)