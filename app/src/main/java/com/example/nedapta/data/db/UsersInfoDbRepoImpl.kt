package com.example.nedapta.data.db

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UsersInfoDbRepoImpl @Inject constructor(
    private val userDao: UserInfoDao
): UsersInfoDbRepo {
    override suspend fun getUsersInfoListFromDbRepo(): Flow<List<DbUser>> =
        userDao.getUsersInfo()


    override suspend fun getUserInfoFromDbRepo(uuid: String): Flow<DbUser> =
        userDao.getUserInfo(uuid)

    override suspend fun addUsersToDbRepo(users: List<DbUser>) =
        userDao.insertAll(users)

    override suspend fun deleteAllUsersFromDbRepo() =
        userDao.deleteAll()
}
