package com.example.nedapta

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NedapTaApplication: Application() {
}