package com.example.nedapta.di

import android.content.Context
import android.net.ConnectivityManager
import com.example.nedapta.data.DataRepo
import com.example.nedapta.data.DataRepoImpl
import com.example.nedapta.data.remote.ApiServiceBuilder.retrofitBuilder
import com.example.nedapta.data.remote.UserApi
import com.example.nedapta.data.remote.UserListRepo
import com.example.nedapta.data.remote.UserListRepoImpl
import com.example.nedapta.data.db.UserInfoDao
import com.example.nedapta.data.db.UsersInfoDbRepoImpl
import com.example.nedapta.data.db.UsersInfoDB
import com.example.nedapta.data.db.UsersInfoDbRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideUserRepo(
        api: UserApi
    ): UserListRepo {
        return UserListRepoImpl(api)
    }

    @Provides
    @Singleton
    fun provideUserApi(): UserApi {
        return retrofitBuilder
            .build()
            .create(UserApi::class.java)
    }

    @Provides
    @Singleton
    fun provideUserInfoDb(@ApplicationContext context :Context) =
        UsersInfoDB.getDatabase(context)

    @Provides
    @Singleton
    fun provideUserInfoDao(usersInfoDB: UsersInfoDB) = usersInfoDB.userDao()

    @Provides
    @Singleton
    fun provideDbRepo(
        userInfoDao: UserInfoDao
    ): UsersInfoDbRepo {
        return UsersInfoDbRepoImpl(userInfoDao)
    }

    @Provides
    fun provideDataRepo(
        apiRepo: UserListRepo,
        dbRepo: UsersInfoDbRepo
    ): DataRepo {
        return DataRepoImpl(
            apiRepo = apiRepo,
            dbRepo = dbRepo
        )
    }

    @Provides
    @Singleton
    fun provideConnectivityManager(@ApplicationContext context: Context): ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
}
