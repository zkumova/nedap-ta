package com.example.nedapta.ui.navigation

import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.nedapta.ui.detailView.UserDetailsScreen
import com.example.nedapta.ui.largeImageScreen.LargeImageScreen
import com.example.nedapta.ui.userList.UserListScreen
import com.example.nedapta.utils.Constants.NAV_GENERAL_ERROR_MESSAGE
import com.example.nedapta.utils.SimpleCenteredText
import kotlinx.coroutines.CoroutineScope

@Composable
fun NedapNavigation(
    scope: CoroutineScope,
    state: ScaffoldState
) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.ListScreen.route) {
        composable(Screen.ListScreen.route){
            UserListScreen(navController = navController)
        }
        composable(
            route = Screen.DetailsScreen.route + "/{userId}",
            arguments = listOf(
                navArgument("userId"){
                    type = NavType.StringType
                }
            )
        ) {
            val userId = remember {
                it.arguments?.getString("userId")
            }
            if (userId != null) {
                UserDetailsScreen(
                    uuid = userId,
                    navController = navController
                )
            } else {
                SimpleCenteredText(NAV_GENERAL_ERROR_MESSAGE)
            }
        }
        composable(
            route = Screen.ImageScreen.route + "/{uuid}",
            arguments = listOf(
                navArgument("uuid"){
                    type = NavType.StringType
                }
            )
        ) {
            val uuid = remember {
                it.arguments?.getString("uuid")
            }
            if (uuid != null) {
                LargeImageScreen(
                    uuid = uuid,
                    navController = navController
                )
            } else {
                SimpleCenteredText(NAV_GENERAL_ERROR_MESSAGE)
            }
        }
    }
}
