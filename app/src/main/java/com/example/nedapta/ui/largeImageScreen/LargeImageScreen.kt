package com.example.nedapta.ui.largeImageScreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Face
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.Dimension
import androidx.constraintlayout.compose.layoutId
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.nedapta.ui.detailView.UserDetailViewModel
import com.example.nedapta.utils.Constants
import com.example.nedapta.utils.SimpleCenteredText
import com.example.nedapta.utils.UiStateWrapper

@Composable
fun LargeImageScreen(
    uuid: String,
    navController: NavController,
) {

    val viewModel: UserDetailViewModel = hiltViewModel()
    val userUiState by viewModel.userState.collectAsStateWithLifecycle()

    LaunchedEffect(key1 = uuid) {
        viewModel.loadUserInfo(uuid)
    }

    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceAround
    ) {
        UiStateWrapper(uiState = userUiState) {user ->
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth(0.9f)
                    .fillMaxHeight(0.9f),
                model = user.imageLarge,
                placeholder = rememberVectorPainter(image = Icons.Default.Face),
                error = rememberVectorPainter(image = Icons.Default.Clear),
                contentDescription = user.name + "picture",
                alignment = Alignment.Center
            )
        }
        Box(
            modifier = Modifier
                .height(80.dp)
                .fillMaxWidth()
        ) {
            SimpleCenteredText(message = "Gorgeous, aren't they?")
        }
    }
}