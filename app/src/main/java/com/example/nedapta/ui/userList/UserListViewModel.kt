package com.example.nedapta.ui.userList

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nedapta.data.DataRepo
import com.example.nedapta.data.db.DbUser
import com.example.nedapta.utils.Constants.LIST_AMOUNT
import com.example.nedapta.utils.UiState
import com.example.nedapta.utils.asUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserListViewModel @Inject constructor(
    private val repo: DataRepo,
    private val connectivityManager: ConnectivityManager
) : ViewModel() {

    private val _userInfoState =
        MutableStateFlow<UiState<List<DbUser>>>(UiState.Loading)
    val userInfoState: StateFlow<UiState<List<DbUser>>> get() = _userInfoState
    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing: StateFlow<Boolean> get() = _isRefreshing

    private var lastFetchJob: Job? = null

    init {
        loadUserListInfo()
    }

    fun refreshData() {
        loadUserListInfo()
        viewModelScope.launch { _isRefreshing.emit(false) }
    }


    private fun loadUserListInfo() {
        lastFetchJob?.cancel()
        lastFetchJob = viewModelScope.launch(Dispatchers.IO) {
            repo.getUsersData(
                amount = LIST_AMOUNT,
                isConnectedToNetwork = isConnectedToNetwork(connectivityManager)
            )
                .asUiState()
                .collect { state -> _userInfoState.value = state }
        }
    }

    private fun isConnectedToNetwork(connectivityManager: ConnectivityManager): Boolean {
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network) ?: return false

        return capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
    }
}
