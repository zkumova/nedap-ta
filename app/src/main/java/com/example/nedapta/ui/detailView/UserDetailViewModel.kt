package com.example.nedapta.ui.detailView

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nedapta.data.DataRepo
import com.example.nedapta.data.db.DbUser
import com.example.nedapta.utils.UiState
import com.example.nedapta.utils.asUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserDetailViewModel @Inject constructor(
    private val repo: DataRepo
) : ViewModel() {

    private val _userState =
        MutableStateFlow<UiState<DbUser>>(UiState.Loading)
    val userState: StateFlow<UiState<DbUser>> get() = _userState

    private var lastFetchJob: Job? = null

    fun loadUserInfo(id: String) {
        lastFetchJob?.cancel()
        lastFetchJob = viewModelScope.launch(Dispatchers.IO) {
            repo.getUserData(id)
                .asUiState()
                .collect { state -> _userState.value = state }
        }
    }
}