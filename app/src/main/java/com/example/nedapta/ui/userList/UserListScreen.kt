package com.example.nedapta.ui.userList

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.nedapta.ui.navigation.Screen
import com.example.nedapta.ui.theme.Purple80
import com.example.nedapta.utils.Constants.LOG_TAG
import com.example.nedapta.utils.UiStateWrapper


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun UserListScreen(
    navController: NavController
) {

    val listViewModel: UserListViewModel = hiltViewModel()
    val users by listViewModel.userInfoState.collectAsStateWithLifecycle()
    val isRefreshing by listViewModel.isRefreshing.collectAsStateWithLifecycle()
    val pullRefreshState = rememberPullRefreshState(
        refreshing = isRefreshing,
        onRefresh = { listViewModel.refreshData() })

    Box(
        modifier = Modifier
            .fillMaxSize()
            .pullRefresh(pullRefreshState)
    ) {
        Log.d(LOG_TAG, "inside the box")
        UiStateWrapper(users) { list ->
            Log.d(LOG_TAG, "inside the UiStateWrapper")
            list.forEach {
                Log.d(LOG_TAG, it.name)
            }
            LazyVerticalGrid(
                columns = GridCells.Adaptive(150.dp),
                modifier = Modifier.padding(5.dp)
            )
            {
                Log.d(LOG_TAG, "inside the LazyVerticalGrid")
                items(list) { user ->
                    Log.d(LOG_TAG, "inside the items, userInfoName = ${user.name}")
                    ImageCard(
                        modifier = Modifier
                            .padding(10.dp)
                            .clickable {
                                navController.navigate(Screen.DetailsScreen.withArgs(user.id))
                            },
                        imageUrl = user.imageSmall,
                        name = user.name,
                        location = user.location
                    )
                }
            }
            PullRefreshIndicator(
                isRefreshing,
                pullRefreshState,
                Modifier.align(Alignment.TopCenter)
            )
        }
    }
}

@Composable
fun ImageCard(
    imageUrl: String,
    name: String,
    location: String,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier.fillMaxWidth(),
        shape = RoundedCornerShape(15.dp),
        elevation = 5.dp
    ) {
        Box(
            modifier = Modifier
                .height(200.dp)
                .width(150.dp)
        ) {
            AsyncImage(
                model = imageUrl,
                placeholder = rememberVectorPainter(image = Icons.Default.Face),
                error = rememberVectorPainter(image = Icons.Default.Clear),
                contentDescription = "$name + picture",
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                MaterialTheme.colors.primary
                            ),
                            startY = 200f
                        )
                    )
            )
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(12.dp),
                verticalArrangement = Arrangement.Bottom
            ) {
                Text(
                    text = name,
                    color = MaterialTheme.colors.onPrimary,
                    style = MaterialTheme.typography.body1
                )
                Text(
                    text = location,
                    color = MaterialTheme.colors.onSecondary,
                    style = MaterialTheme.typography.caption
                )
            }
        }
    }
}