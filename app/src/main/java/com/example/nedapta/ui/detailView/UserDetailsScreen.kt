package com.example.nedapta.ui.detailView

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Face
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import androidx.constraintlayout.compose.Dimension
import androidx.constraintlayout.compose.layoutId
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.nedapta.ui.navigation.Screen
import com.example.nedapta.utils.Constants.ID_AGE
import com.example.nedapta.utils.Constants.ID_BOTTOM_TEXT
import com.example.nedapta.utils.Constants.ID_EMAIL
import com.example.nedapta.utils.Constants.ID_IMAGE
import com.example.nedapta.utils.Constants.ID_LOCATION
import com.example.nedapta.utils.Constants.ID_NAME
import com.example.nedapta.utils.Constants.ID_NATIONALITY
import com.example.nedapta.utils.Constants.LOG_TAG
import com.example.nedapta.utils.SimpleCenteredText
import com.example.nedapta.utils.UiStateWrapper

/**
 * Used [ConstraintLayout] as the way to show different approaches to build screens
 * using compose. It is recommended to use general compose functions to create desired design,
 * but in a case of complex logic it is beneficial to lean to [ConstraintLayout] structure.
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun UserDetailsScreen(
    uuid: String,
    navController: NavController,
) {

    val viewModel: UserDetailViewModel = hiltViewModel()
    val userUiState by viewModel.userState.collectAsStateWithLifecycle()

    LaunchedEffect(key1 = uuid) {
        viewModel.loadUserInfo(uuid)
    }

    val constraints = ConstraintSet {
        val name = createRefFor(ID_NAME)
        val age = createRefFor(ID_AGE)
        val email = createRefFor(ID_EMAIL)
        val nationality = createRefFor(ID_NATIONALITY)
        val location = createRefFor(ID_LOCATION)
        val image = createRefFor(ID_IMAGE)
        val bottomBox = createRefFor(ID_BOTTOM_TEXT)

        constrain(image) {
            top.linkTo(parent.top)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.percent(0.65f)
            height = Dimension.ratio("1:1")
        }
        constrain(name) {
            top.linkTo(image.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.wrapContent
            height = Dimension.wrapContent
        }
        constrain(nationality) {
            top.linkTo(name.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.wrapContent
            height = Dimension.wrapContent
        }
        constrain(age) {
            top.linkTo(nationality.top)
            start.linkTo(nationality.end)
            width = Dimension.wrapContent
            height = Dimension.wrapContent
        }
        createHorizontalChain(nationality, age, chainStyle = ChainStyle.Packed)
        constrain(location) {
            top.linkTo(nationality.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.fillToConstraints
            height = Dimension.wrapContent
        }
        constrain(email) {
            top.linkTo(location.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.fillToConstraints
            height = Dimension.wrapContent
        }
        constrain(bottomBox) {
            bottom.linkTo(parent.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            width = Dimension.fillToConstraints
            height = Dimension.value(100.dp)
        }
    }

    ConstraintLayout(
        constraints,
        modifier = Modifier.fillMaxSize()
    ) {
        UiStateWrapper(uiState = userUiState) { user ->
            Log.d(LOG_TAG, user.nationality)
            Card(
                modifier = Modifier
                    .layoutId(ID_IMAGE)
                    .padding(start = 16.dp, top = 16.dp, end = 16.dp),
                shape = RoundedCornerShape(15.dp),
                elevation = 5.dp,
                onClick = {
                    navController.navigate(
                        Screen.ImageScreen.withArgs(
                            user.id
                        )
                    )
                }
            ) {
                AsyncImage(
                    model = user.imageMedium,
                    placeholder = rememberVectorPainter(image = Icons.Default.Face),
                    error = rememberVectorPainter(image = Icons.Default.Clear),
                    contentDescription = user.name + "picture",
                    contentScale = ContentScale.Crop
                )
            }
            Text(
                text = user.name,
                modifier = Modifier
                    .layoutId(ID_NAME)
                    .padding(top = 16.dp),
                style = MaterialTheme.typography.subtitle1

            )
            Text(
                text = user.age.toString(),
                modifier = Modifier
                    .layoutId(ID_AGE)
                    .padding(start = 16.dp),
                style = MaterialTheme.typography.body1
            )
            Text(
                text = user.email,
                modifier = Modifier.layoutId(ID_EMAIL),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.body1
            )
            Text(
                text = user.nationality + ",",
                modifier = Modifier.layoutId(ID_NATIONALITY),
                style = MaterialTheme.typography.body1
            )
            Text(
                text = user.location,
                modifier = Modifier.layoutId(ID_LOCATION),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.body1
            )
            Box(
                modifier = Modifier
                    .layoutId(ID_BOTTOM_TEXT)
                    .background(MaterialTheme.colors.secondary)
            ) {
                SimpleCenteredText(message = "More data avaliable soon!")
            }
        }
    }
}