package com.example.nedapta.ui.navigation

sealed class Screen(val route: String) {
    data object ListScreen: Screen("user_list_screen")
    data object DetailsScreen: Screen("user_detail_screen")
    data object ImageScreen: Screen("large_image_screen")

    fun withArgs(vararg args: String): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}
