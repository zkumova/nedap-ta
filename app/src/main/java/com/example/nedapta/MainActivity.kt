package com.example.nedapta

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.rememberCoroutineScope
import com.example.nedapta.ui.navigation.NedapNavigation
import com.example.nedapta.ui.settings.AppBar
import com.example.nedapta.ui.settings.DrawerBody
import com.example.nedapta.ui.settings.DrawerHeader
import com.example.nedapta.ui.theme.NedapTATheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NedapTATheme {
                val scaffoldState = rememberScaffoldState()
                val scope = rememberCoroutineScope()
                Scaffold(
                    scaffoldState = scaffoldState,
                    topBar = {
                        AppBar(
                            onNavigationItemClick = {
                                scope.launch {
                                    scaffoldState.drawerState.open()
                                }
                            })
                    },
                    drawerContent = {
                        DrawerHeader()
                        DrawerBody(
                            scope = scope,
                            state = scaffoldState,
                            itemTextStyle = MaterialTheme.typography.body1
                        )
                    }
                ) {
                    NedapNavigation(
                        scope = scope,
                        state = scaffoldState
                    )
                }
            }
        }
    }
}
